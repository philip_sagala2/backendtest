<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use View;
use App\User;

class UserController extends Controller
{
    public function index()
    {
    	$data = User::get();
    	return View::make('index', ['data' => $data]);
    }

    public function show($id)
    {
    	$data = User::find($id)->first();
    	return View::make('detail', ['data' => $data]);
    }
}
