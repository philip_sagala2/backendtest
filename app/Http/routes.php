<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('', ['as' => 'home', 'uses' => 'UserController@index']);

Route::post('create', ['as' => 'create', 'uses' => 'UserController@create']);
Route::post('edit', ['as' => 'edit', 'uses' => 'UserController@edit']);

Route::get('delete/{id}', ['as' => 'delete', 'uses' => 'UserController@delete']);
Route::get('detail/{id}', ['as' => 'view.detail', 'uses' => 'UserController@show']);