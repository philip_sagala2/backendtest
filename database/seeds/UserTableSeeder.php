<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dt = Carbon::now('Asia/Jakarta');
		DB::table('users')->truncate();
		DB::table('users')->insert(array(
			array(
        		'name' => 'John Doe',
        		'email' => 'john@doe.com',
        		'password' => '12345',
        		'remember_token' => 'x12345612345',
      			'created_at' => $dt,
                'updated_at' => $dt
      		)
		));
    }
}
