# Backend Test

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

**Persyaratan dan Spesifikasi**

- Apache Server
- MySQL
- PHP
- Composer
- GIT

**Cara Setup**

1.Buat file .env pada root folder pada project dan copy konfigurasi berikut ini, jangan lupa untuk merubah konfigurasi database MySQL sesuai dengan yang ada pada mesin anda.


```
#!text

APP_ENV=local
APP_DEBUG=true
APP_KEY=1ed64Avfp2y9xj97Sr2ICBvdjnznKK9R

DB_HOST=localhost
DB_DATABASE=backendtestDB
DB_USERNAME=root
DB_PASSWORD=

CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync
63275820
MAIL_DRIVER=smtp
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

```

2.Buka console/terminal lalu lakukan eksekusi kode berikut dibawah ini, pastikan sebelumnya anda sudah memiliki instalasi Laravel framework pada mesin anda. Silahkan cek [Laravel website](http://laravel.com/docs) 


```
#!text

composer install
php artisan migrate
php artisan db:seed
```


3.Tugas-tugasnya

- Anda dituntut untuk membuat fungsi CRUD pada saat ini sudah ada fungsi Read sebagai contoh.
- Jika ada fungsi-fungsi yang hendak ditambahkan atau dilengkapi silahkan ditambahkan.
- Jika ada sesuatu yang kurang berkenan menurut pengalaman anda kode tersebut dapat anda rubah sesuai kebutuhan.
- Gunakan CSRF pada waktu create dan edit user

4.Kriteria Penilaian

- Anda sebagai kandidat backend programmer dituntut untuk memahami PHP khususnya framework Laravel.
- Anda juga dituntut untuk memahami konsep MVC dan OOP dalam mengerjakan soal ini.

5.Nilai Tambah

- Jika anda dapat menambahkan fungsi search.
- Jika anda dapat menambahkan fungsi halaman, 1 halaman berisi 5 baris.
- Jika anda dapat menambahkan validasi pada saat create dan edit user.

Setelah selesai mengerjakan silahkan kirimkan melalui email file2x apa saja yang berubah pada proses pengerjaan kepada HRD kami.

Selamat mengerjakan, Sukses ya.