<style type="text/css">

	table {
		margin: 0px auto;
	}
</style>

<div>
<a href="" style="margin-left:70%">Create</a>
</div>
<table border="1">
	<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Password</th>
			<th>Remember Token</th>
			<th>Create At</th>
			<th>Update At</th>
			<th colspan="2">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($data as $d)
		<tr>
			<td><a href="{{ URL::route('view.detail', ['id' => $d->id]) }}">{{$d->name}}</a></td>
			<td>{{$d->email}}</td>
			<td>{{$d->password}}</td>
			<td>{{$d->remember_token}}</td>
			<td>{{$d->created_at}}</td>
			<td>{{$d->updated_at}}</td>
			<td><a href="">Edit</a></td>
			<td><a href="">Delete</a></td>
		</tr>
		@endforeach
	</tbody>
</table>
